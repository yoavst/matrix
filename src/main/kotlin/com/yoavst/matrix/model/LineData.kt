package com.yoavst.matrix.model

data class LineData<out K>(val line: Int, val value: K)