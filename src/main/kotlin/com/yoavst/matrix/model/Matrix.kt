package com.yoavst.matrix.model


abstract class Matrix<K>(val width: Int, val height: Int) {
    operator fun get(row: Int, column: Int): K = get(width * row + column)
    abstract operator fun get(index: Int): K

    fun getFormatted(row: Int, column: Int): String = getFormatted(width * row + column)
    abstract fun getFormatted(index: Int): String

    operator fun set(row: Int, column: Int, value: K) {
        set(width * row + column, value)
    }
    protected abstract operator fun set(index: Int, value: K)

    operator fun set(row: Int, column: Int, value: String) {
        set(width * row + column, value)
    }
    protected abstract fun set(index: Int, value: String)

    abstract fun apply(newRows: Array<Array<LineData<K>>>): Matrix<K>

    abstract fun allow(value: String): Boolean

    fun copy(matrix: Matrix<K>) {
        for (i in 0..width * height - 1) {
            matrix[i] = get(i)
        }
    }
}