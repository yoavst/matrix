package com.yoavst.matrix.model

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.regex.Pattern

class DecimalMatrix(width: Int, height: Int) : Matrix<BigDecimal>(width, height) {
    val array = Array<BigDecimal>(width * height) { BigDecimal.ZERO }

    override fun getFormatted(index: Int): String {
        val number = array[index]
        if (number.compareTo(BigDecimal.ZERO) == 0) return "0"
        else return number.format()
    }

    override fun get(index: Int): BigDecimal = array[index]
    override fun set(index: Int, value: BigDecimal) {
        array[index] = value
    }

    override fun set(index: Int, value: String) {
        array[index] = BigDecimal(value)
    }

    override fun allow(value: String): Boolean = DOUBLE_PATTERN.matcher(value).matches()
    override fun apply(newRows: Array<Array<LineData<BigDecimal>>>): Matrix<BigDecimal> {
        val matrix = DecimalMatrix(width, height)
        copy(matrix)
        newRows.forEachIndexed { index, changes ->
            if (changes.isNotEmpty()) {
                val row = arrayOfNulls<BigDecimal>(width)
                for ((rowNumber, value) in changes) {
                    repeat(width) {
                        row[it] = (row[it] ?: BigDecimal.ZERO) + get(rowNumber - 1, it) * value
                    }
                }
                repeat(width) {
                    matrix[index, it] = row[it] ?: BigDecimal.ZERO
                }
            }
        }
        return matrix
    }

    companion object {
        private val DOUBLE_PATTERN = Pattern.compile(
                "[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)" + "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|" + "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))" + "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*"
        )

        private val formatter = DecimalFormat("#.######")
        private fun Number.format(): String = formatter.format(this)

    }
}

