package com.yoavst.matrix

import com.aquafx_project.AquaFx
import com.aquafx_project.AquaFxStyle
import com.aquafx_project.controls.skin.styles.ButtonType
import com.aquafx_project.controls.skin.styles.MacOSDefaultIcons
import com.guigarage.stylemanager.ApplicationStyleManager
import com.yoavst.matrix.controller.AbstractController
import com.yoavst.matrix.controller.DecimalController
import javafx.application.Application
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.TextField
import javafx.scene.control.ToolBar
import javafx.scene.image.Image
import javafx.scene.input.*
import javafx.scene.layout.Priority
import javafx.stage.Stage
import kotlinfx.builders.*
import kotlin.properties.Delegates

fun main(args: Array<String>) {
    Application.launch(App::class.java)
}

class App : Application() {
    private val matrixWidth get() = controller.matrixWidth
    private val matrixHeight get() = controller.matrixHeight

    private var cells: Array<TextField> by Delegates.notNull()
    private var orders: Array<TextField> by Delegates.notNull()

    private val message = TextField {}
    private lateinit var table: Node

    private lateinit var controller: AbstractController<*>

    private val btnUndo = Button {
        isDisable = true
        AquaFx.createButtonStyler().setIcon(MacOSDefaultIcons.LEFT).setType(ButtonType.LEFT_PILL).style(this)
        setOnAction {
            goBack()
            it.consume()
        }
    }
    private val btnRedo = Button {
        isDisable = true
        AquaFx.createButtonStyler().setIcon(MacOSDefaultIcons.RIGHT).setType(ButtonType.RIGHT_PILL).style(this)
        setOnAction {
            goNext()
            it.consume()
        }
    }

    val btnRestore = Button("Restore") {
        isDisable = true
        AquaFx.createButtonStyler().setType(ButtonType.RIGHT_PILL).style(this)
        setOnAction {
            it.consume()
            cleanHistory()
        }
    }

    fun cleanHistory() {
        controller.restoreMatrix()
        updateBoard(Array(controller.matrixHeight) { "" })
    }

    fun goBack() {
        val actions = controller.goBack()
        if (actions != null)
            updateBoard(actions)
    }

    fun goNext() {
        val actions = controller.goNext()
        if (actions != null)
            updateBoard(actions)
    }

    fun updateBoard(actions: Array<String>? = null) {
       repeat(cells.size) {
            val text = controller.getFormatted(it)
            if (text == "0") cells[it].text = ""
            else cells[it].text = text
        }
        actions?.forEachIndexed { i, s -> orders[i].text = s }
        btnUndo.isDisable = !controller.supportUndo
        btnRedo.isDisable = !controller.supportRedo
        message.text = ""
    }

    override fun start(stage: Stage) {
        controller = DecimalController()
        controller.init()
        Stage(stage, title = "Matrix Helper") {
            scene = Scene(width = 600.0, height = 400.0) {
                root = VBox {
                    table = createTable()
                    +createToolbar()
                    +space(height = 10.0)
                    +HBox {
                        val rows = TextField {
                            prefWidth = 30.0
                            alignment = Pos.CENTER
                            promptText = "3"
                        }.forIntegers()
                        val columns = TextField {
                            prefWidth = 30.0
                            alignment = Pos.CENTER
                            promptText = "3"
                        }.forIntegers()
                        val apply = Button("Apply") {
                            AquaFx.createButtonStyler().setType(ButtonType.ROUND_RECT).style(this)
                            setOnAction {
                                it.consume()
                                if (rows.text.isNotEmpty() || columns.text.isNotEmpty()) {
                                    val newWidth = columns.text or matrixWidth.toString()
                                    val newHeight = rows.text or matrixHeight.toString()
                                    if (!(newWidth.toInt() == matrixWidth && newHeight.toInt() == matrixHeight)) {
                                        controller.resize(newWidth.toInt(), newHeight.toInt())
                                        btnRestore.isDisable = true
                                        this@VBox.children.remove(table)
                                        table = createTable()
                                        this@VBox.children.add(3, table)
                                        updateBoard(Array(controller.matrixHeight) { "" })
                                        message.text = ""
                                    }

                                }
                            }
                        }
                        +space(width = 10.0) + Label("Rows") + space(width = 5.0) + rows
                        +space(width = 10.0) + Label("Columns") + space(width = 5.0) + columns
                        +spaceTaker() + apply + space(width = 10.0)
                    }
                    +table
                    +space(height = 10.0)
                    +verticalSpaceTaker()
                    +message
                }
            }
            ApplicationStyleManager.getInstance().styleApplication(AquaFxStyle()) // Fix bug of jar
            scene.accelerators.put(KeyCodeCombination(KeyCode.F10), Runnable {
                run()
            })
            stage.icons.addAll(
                    Image(Thread.currentThread().contextClassLoader.getResourceAsStream("logo_48.png")),
                    Image(Thread.currentThread().contextClassLoader.getResourceAsStream("logo_64.png")))

        }.show()
        updateBoard()
    }

    fun run() {
        controller.run(orders.map { it.text.trim() }.toTypedArray())
        updateBoard(Array(matrixHeight) { "" })
    }


    fun createToolbar(): ToolBar = ToolBar {
        val btnRun = Button("run") {
            AquaFx.createButtonStyler().setType(ButtonType.ROUND_RECT).style(this)
            setOnAction {
                it.consume()
                run()
            }
        }
        val btnSave = Button("Save") {
            AquaFx.createButtonStyler().setType(ButtonType.LEFT_PILL).style(this)
            setOnAction {
                it.consume()
                controller.saveMatrix()
                btnRestore.isDisable = false
            }
        }
        val btnClear = Button("Clear") {
            AquaFx.createButtonStyler().setType(ButtonType.ROUND_RECT).style(this)
            setOnAction {
                it.consume()
                controller.clearMatrix()
                updateBoard(Array(matrixHeight) { "" })
                message.text = ""
            }
        }

        val btnShare = Button {
            AquaFx.createButtonStyler().setIcon(MacOSDefaultIcons.SHARE).style(this)
            setOnAction {
                it.consume()
                val content = ClipboardContent()
                content.putString(controller.exportHistory())
                Clipboard.getSystemClipboard().setContent(content)
                message.text = "Copied to clipboard"
            }
        }

        val btnHelp = Button("?") {
            AquaFx.createButtonStyler().setType(ButtonType.HELP).style(this)
        }

        items.addAll(btnUndo, btnRedo, getSeparator(), btnRun, getSmallSeparator(), btnClear,
                getSeparator(), btnSave, btnRestore, getSeparator(), btnShare, spaceTaker(), btnHelp)
    }

    fun createTable(): Node = GridPane(padding = Insets(10.0)) {
        hgap = matrixWidth.toDouble()
        vgap = matrixHeight.toDouble()
        prefWidth = 156 + (94 * matrixWidth).toDouble()
        var k = 0
        val cellsTemp = arrayOfNulls<TextField>(matrixWidth * matrixHeight)
        val ordersTemp = arrayOfNulls<TextField>(matrixHeight)
        for (w in 0..matrixWidth) {
            for (h in 0..matrixHeight) {
                if (w == 0) {
                    if (h == 0) {
                        add(Label("") {
                            prefWidth = 156.0
                        }, 0, 0)
                    } else {
                        val field = TextField("") {
                            promptText = "R$h"
                            alignment = Pos.CENTER_LEFT
                            prefWidth = 156.0
                            textProperty().addListener { _, old, new ->
                                if (new == " ") text = ""
                                else if (new.length > old.length) {
                                    val change = new.last()
                                    if (change == 'r') text = old + 'R'
                                    else if (!change.isLetterOrDigit() && change !in possibleCharsForOrder) {
                                        text = old
                                    }
                                }
                            }
                            addEventFilter(KeyEvent.KEY_PRESSED) {
                                when (it.code) {
                                    KeyCode.TAB -> {
                                        if (it.isShiftDown) {
                                            if (h != 1)
                                                cells[(h - 1) * matrixWidth - 1].requestFocus()
                                        } else {
                                            cells[(h - 1) * matrixWidth].requestFocus()
                                        }
                                        it.consume()
                                    }
                                    KeyCode.UP -> {
                                        if (h != 1)
                                            orders[h - 2].requestFocus()

                                        it.consume()
                                    }
                                    KeyCode.DOWN -> {
                                        if (h != matrixHeight)
                                            orders[h].requestFocus()
                                        it.consume()
                                    }
                                    KeyCode.RIGHT -> {
                                        if (selection.length != 0) {
                                            val caretPositionNew = selection.end
                                            selectRange(0, 0)
                                            positionCaret(caretPositionNew)
                                            it.consume()
                                        } else if (text == "" || caretPosition == text.length) {
                                            cells[(h - 1) * matrixWidth].requestFocus()
                                            it.consume()
                                        }
                                    }
                                    KeyCode.LEFT -> {
                                        if (selection.length != 0) {
                                            val caretPositionNew = selection.start
                                            selectRange(0, 0)
                                            positionCaret(caretPositionNew)
                                            it.consume()
                                        } else if (h != 1 && (text == "" || caretPosition == 0)) {
                                            cells[(h - 1) * matrixWidth - 1].requestFocus()
                                            it.consume()
                                        }
                                    }
                                    KeyCode.ESCAPE -> {
                                        selectRange(0, 0)
                                        it.consume()
                                    }
                                    else -> Unit
                                }
                            }
                        }
                        ordersTemp[k] = field
                        k++
                        add(field, 0, h)
                    }
                } else if (h == 0) {
                    if (w == matrixWidth) {
                        add(Label("Constant") {
                            prefWidth = 94.0
                            alignment = Pos.CENTER
                        }, w, 0)
                    } else add(Label("X$w") {
                        prefWidth = 94.0
                        alignment = Pos.CENTER
                    }, w, 0)
                } else {
                    val field = TextField {
                        promptText = "0"
                        alignment = Pos.CENTER
                        prefWidth = 94.0
                        textProperty().addListener { _, old, new ->
                            if (new == "-") controller.data[h - 1, w - 1] = "-1"
                            else if (new == "0") text = ""
                            else if (new.isEmpty() || controller.data.allow(new)) {
                                controller.data[h - 1, w - 1] = new.or("0")
                            } else text = old
                        }
                        addEventFilter(KeyEvent.KEY_PRESSED) {
                            when (it.code) {
                                KeyCode.DOWN -> {
                                    if (h != matrixHeight)
                                        cells[h * matrixWidth + w - 1].requestFocus()
                                    it.consume()
                                }
                                KeyCode.UP -> {
                                    if (h != 1) {
                                        cells[(h - 2) * matrixWidth + w - 1].requestFocus()
                                    }
                                    it.consume()
                                }
                                KeyCode.TAB -> {
                                    if (it.isShiftDown) {
                                        if (w == 1)
                                            orders[h - 1].requestFocus()
                                        else cells[(h - 1) * matrixWidth + w - 2].requestFocus()
                                    } else {
                                        if (w == matrixWidth) {
                                            if (h != matrixHeight)
                                                orders[h].requestFocus()
                                        } else
                                            cells[(h - 1) * matrixWidth + w].requestFocus()
                                    }
                                    it.consume()
                                }
                                KeyCode.RIGHT -> {
                                    if (selection.length != 0) {
                                        val caretPositionNew = selection.end
                                        selectRange(0, 0)
                                        positionCaret(caretPositionNew)
                                        it.consume()
                                    } else if (text == "" || caretPosition == text.length) {
                                        if (w == matrixWidth) {
                                            if (h != matrixHeight)
                                                orders[h].requestFocus()
                                        } else
                                            cells[(h - 1) * matrixWidth + w].requestFocus()
                                        it.consume()
                                    }
                                }
                                KeyCode.LEFT -> {
                                    if (selection.length != 0) {
                                        val caretPositionNew = selection.start
                                        selectRange(0, 0)
                                        positionCaret(caretPositionNew)
                                        it.consume()
                                    } else if (text == "" || caretPosition == 0) {
                                        if (w == 1)
                                            orders[h - 1].requestFocus()
                                        else cells[(h - 1) * matrixWidth + w - 2].requestFocus()
                                        it.consume()
                                    }
                                }
                                KeyCode.ESCAPE -> {
                                    selectRange(0, 0)
                                    it.consume()
                                }

                                else -> Unit
                            }
                        }
                    }
                    cellsTemp[(h - 1) * matrixWidth + w - 1] = field
                    add(field, w, h)
                }
            }
        }
        cells = cellsTemp.requireNoNulls()
        orders = ordersTemp.requireNoNulls()
    }

    infix fun String?.or(value: String): String = if (isNullOrBlank()) value else this!!


    fun getSeparator(): Node = space(width = 15.0)
    fun getSmallSeparator(): Node = space(width = 7.5)
    fun space(width: Double = 1.0, height: Double = 1.0): Node = HBox { setPrefSize(width, height) }
    fun spaceTaker(): Node = HBox { javafx.scene.layout.HBox.setHgrow(this, Priority.ALWAYS) }
    fun verticalSpaceTaker(): Node = VBox { javafx.scene.layout.VBox.setVgrow(this, Priority.ALWAYS) }

    fun TextField.forIntegers(): TextField {
        textProperty().addListener { _, old, new ->
            if (new.length > 2) text = old
            else if (new == "0") text = ""
            else if (new.isNotEmpty() && new.length > old.length) {
                val char = new.last()
                if (char < '0' || char > '9')
                    text = old
            }
        }
        return this
    }

    val possibleCharsForOrder = charArrayOf('R', ' ', '-', '+', '.', '(', ')', '/')


}
