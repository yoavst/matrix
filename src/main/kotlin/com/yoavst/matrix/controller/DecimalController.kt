package com.yoavst.matrix.controller

import com.yoavst.matrix.Expression
import com.yoavst.matrix.model.DecimalMatrix
import com.yoavst.matrix.model.Matrix
import java.math.BigDecimal


class DecimalController: AbstractController<BigDecimal>() {
    override fun getNewData(width: Int, height: Int): Matrix<BigDecimal> = DecimalMatrix(width, height)

    override fun parse(data: String): BigDecimal = Expression(data).eval()
}