package com.yoavst.matrix.controller

import com.yoavst.matrix.Expression
import com.yoavst.matrix.MatrixExporter
import com.yoavst.matrix.model.Code
import com.yoavst.matrix.model.LineData
import com.yoavst.matrix.model.Matrix
import java.util.*


abstract class AbstractController<K> {
    private val history: ArrayList<Matrix<K>> = ArrayList()
    private val historyActions: ArrayList<Array<String>> = ArrayList()
    private var index = -1

    lateinit var data: Matrix<K>
    private var savedData: Matrix<K>? = null

    val matrixWidth: Int
        get() {
            return data.width
        }
    val matrixHeight: Int
        get() {
            return data.height
        }
    val supportUndo: Boolean
        get() {
            return index != 0
        }
    val supportRedo: Boolean
        get() {
            return index + 1 < history.size
        }

    fun exportHistory(): String {
        return MatrixExporter.export(history, historyActions)
    }

    abstract fun getNewData(width: Int, height: Int): Matrix<K>

    fun getFormatted(index: Int) = data.getFormatted(index)

    fun saveMatrix() {
        savedData = data
    }

    fun restoreMatrix() {
        cleanHistory(savedData!!)
    }

    fun clearMatrix() {
        cleanHistory(getNewData(matrixWidth, matrixHeight), Array(matrixHeight) { "" })
    }

    fun init() {
        cleanHistory(getNewData(3, 3))
    }

    fun cleanHistory(newMatrix: Matrix<K>, actions: Array<String>? = null) {
        index = 0
        history.clear()
        history.add(newMatrix)
        data = newMatrix
        val newActions: Array<String> = actions ?: Array(newMatrix.height) { "" }
        historyActions.clear()
        historyActions.add(newActions)
    }

    fun addHistory(newMatrix: Matrix<K>, actions: Array<String>) {
        if (index + 1 != history.size) {
            for (i in history.size - 1 downTo index + 1) {
                history.removeAt(i)
                historyActions.removeAt(i)
            }
        }
        data = newMatrix
        index++
        history.add(data)
        historyActions.add(actions)
    }

    fun goBack(): Array<String>? {
        if (index != 0) {
            index--
            data = history[index]
            return historyActions[index + 1]
        }
        return null
    }

    fun goNext(): Array<String>? {
        if (index + 1 != history.size) {
            index++
            data = history[index]
            return if (index + 1 == history.size) Array(matrixHeight) { "" } else historyActions[index + 1]
        } else return null
    }

    fun run(texts: Array<String>): Code {
        try {
            val preData = texts
            for (i in 0..preData.size - 1) {
                val current = (i + 1).toString()
                val text = preData[i]
                if (text.isNotEmpty()) {
                    if (text[0] == '*') preData[i] = text.substring(1) + "R$current}"
                    else if (text == "-") preData[i] = "-R$current}"
                    else if (text == "R$current" || text == "1R$current") {
                        preData[i] = ""
                    }
                }
            }
            val actions = Array(texts.size) { parse(texts[it], it + 1) }
            val isAllEmpty = actions.all { it.isEmpty() }
            if (!isAllEmpty) {
                val newMatrix = data.apply(actions)
                addHistory(newMatrix, texts)
                return Code.Success
            } else return Code.Empty

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return Code.Error
    }

    @Suppress("NAME_SHADOWING")
    fun resize(width: Int, height: Int) {
        val newMatrix = getNewData(width, height)
        val w = Math.min(newMatrix.width, matrixWidth)
        val h = Math.min(newMatrix.height, matrixHeight)
        for (width in 0..w - 1) {
            for (height in 0..h - 1) {
                newMatrix[height, width] = data[height, width]
            }
        }
        savedData = null
        cleanHistory(newMatrix)
    }

    abstract fun parse(data: String): K

    fun parse(text: String, number: Int): Array<LineData<K>> {
        val parts = mutableListOf<LineData<K>>()
        val expression = Expression(text)
        val realParts = expression.expressionTokenizer.asSequence().toList()
        if (realParts.size == 2 && realParts[0] == "*") {
            parts.add(LineData(number,parse(realParts[1])))
        } else if (realParts.size == 1 && realParts[0] == "-")
            parts.add(LineData(number, parse("-1")))
        else if (realParts.size == 2) {
            var part1 = realParts[0]
            if (part1 == "-") part1 = "-1"
            else if (part1 == "+") part1 = "1"
            parts.add(LineData(realParts[1].substring(1).toInt(), parse(part1)))
        } else {
            var string = text.toLowerCase().replace("--".toRegex(), "+").replace("-r".toRegex(), "-1r")
            var pieces = 1
            var rowNum1 = 0
            var rowNum2 = 0
            var coef1 = parse("0")
            var coef2 = parse("0")
            var minusFlag = false
            // need to break the string into two pieces at plus-minus
            var leftPiece = string
            var rightPiece = ""
            var permissionToBreak = false // remains so until "r" encountered
            for (i in 0..string.length - 1) {
                val dig = string[i]
                if (dig == 'r') permissionToBreak = true
                if (((dig == '+') || (dig == '-')) && (permissionToBreak)) {
                    pieces = 2
                    leftPiece = string.substring(0, i)
                    rightPiece = string.substring(i + 1, string.length)
                    if (dig == '-') minusFlag = true
                    break
                }
            }
            for (k in 1..pieces) {
                if (k == 1) string = leftPiece.trim()
                else string = rightPiece.trim()
                for (i in 0..string.length - 1) {
                    if (string[i] == 'r' && k == 1) {
                        if (i == 0) coef1 = parse("1")
                        else coef1 = parse(string.substring(0, i))
                        rowNum1 = Expression(string.right(string.length - i - 1)).eval().toInt()

                    } // end if

                    if (string[i] == 'r' && k == 2) {
                        if (i == 0) coef2 = parse("1")
                        else coef2 = parse(string.substring(0, i))
                        rowNum2 = Expression(string.right(string.length - i - 1)).eval().toInt()
                        if (minusFlag) coef2 = parse("-$coef2")
                    }
                } // next i
            } // next k
            if (coef1.toString() != "0") {
                parts.add(LineData(rowNum1, coef1))
            }
            if (pieces == 2 && coef2.toString() != "0" && rowNum2 != 0) {
                parts.add(LineData(rowNum2, coef2))
            }
        }
        return parts.toTypedArray()
    }

    companion object {
        private fun String.right(num: Int): String {
            return substring(length - num, length)
        }
    }
}