package com.yoavst.matrix

import com.yoavst.matrix.model.Matrix
import java.util.*

object MatrixExporter {
    fun <K> export(data: ArrayList<Matrix<K>>, actions: ArrayList<Array<String>>): String {
        val builder = StringBuilder(data.size * 150)
        data.forEachIndexed { i, matrix ->
            builder.append(createMatrix(matrix))
            if (i != data.size - 1) {
                builder.append("""\xrightarrow{\substack{""")
                actions[i+1].forEachIndexed { position, string ->
                    if (string.isNotEmpty()) builder.append("R${position+1} \\to $string \\\\")
                }
                builder.append(""" }}""", '\n')
            }
        }
        return builder.toString()
    }

    private fun createMatrix(matrix: Matrix<*>): String {
        val builder = StringBuilder(28 + matrix.width * matrix.height * 6)
        builder.append("""\begin{bmatrix}""")
        for (h in 0..matrix.height - 1) {
            builder.append('\n')
            for (w in 0..matrix.width - 1) {
                builder.append(matrix.getFormatted(h, w).or("0"))
                if (w == matrix.width - 1) builder.append(""" \\""")
                else builder.append('&')
            }
        }
        builder.append("""\end{bmatrix}""")
        return builder.toString()
    }

    fun String?.or(value: String): String = if (isNullOrBlank()) value else this!!
}