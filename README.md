# Linear Algebra - Matrix equations helper

**Matrix helper** is a tool that helps to solve linear equations using a matrix.

The tools is based on [KotlinFX](https://github.com/easheb/kotlinfx) library and written entirely in Kotlin.

## Screenshot

![Screenshot](http://i.imgur.com/783L3S3.png)

Download: [Link](https://gitlab.com/yoavst/matrix/blob/updated/matrix.jar)